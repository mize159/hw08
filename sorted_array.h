
#ifndef H_
#define H_
#include <iostream>
#include <stdlib.h>
#include <string>
#include <map>
using namespace std;

template <typename T>
class SortedArray {
public:
	SortedArray() : size_(0), alloc_(0)	{
		values_ = new T[0]; };
	SortedArray(const SortedArray& a) {
		values_ = new T[a.size_];
		values_ = a.values_, size_ = a.size_, alloc_ = a.alloc_;
		
	}
	SortedArray(size_t size) : size_(size), alloc_(size)	{ 
		values_ = new T[size]; };
	~SortedArray(){ 
	}

	SortedArray& operator=(const SortedArray& a)
	{
		values_ = a.values_;
		size_ = a.size_;
		alloc_ = a.alloc_;
		return *this;
	}

	size_t size() const { return size_; }
	const T& operator()(int idx) const		{ return values_[idx]; }

	void Reserve(int size)	{ 
		delete[] values_;
		values_ = new T[size];
		alloc_ = size;
	}  // 주어진 크기만큼 미리 메모리 할당.
	void Add(const T& value)	{ 
		values_[size_] = value;
		size_++;
		alloc_++;
	}

	int Find(const T& value){
		int j = size_+1;
		for (int i = 0; i < size_; i++)
		{
			if (values_[i]==value)	j = i;
		}
		if (j == size_+1)	return -1;
		return j;
	}// 주어진 값의 위치, 없으면 -1을 리턴.

private:
	T* values_;
	int size_, alloc_;

	// operator 함수들을 friend 로 선언하지 말고 public 함수를 이용.
};

template <typename T>
istream& operator>>(istream& is, SortedArray<T>& a)
{	
	int num;
	T buf;
	T* bufarray;
	is >> num;
	a.Reserve(num);
	bufarray = new T[num];
	for (int i= 0; i < num; i++)
	{
		is >> buf;
		bufarray[i] = buf;
	}
	for(int i = 0; i < num; ++i) {
			int min_idx = i;
			for (int j = i + 1; j < num; ++j) {
				if (bufarray[min_idx] > bufarray[j])
					min_idx = j;
			}
			T tmp = bufarray[i];
			bufarray[i] = bufarray[min_idx];
			bufarray[min_idx] = tmp;
	}
	for (int i = 0; i < num; i++)
	{
		a.Add(bufarray[i]);
	}
	delete[] bufarray;
	return is;
}

template <typename T>
ostream& operator<<(ostream& os, const SortedArray<T>& a)
{
	for (int i= 0; i < a.size(); i++)
	{
		os << a.operator()(i) << " ";
	}
	return os;
}

#endif