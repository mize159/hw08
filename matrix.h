#ifndef _h
#define _h
#include <iostream>
#include <stdlib.h>
#include <string>
#include <vector>
using namespace std;


template <typename T>
class Matrix {
public:
	Matrix() : rows_(0), cols_(0)	{};
	Matrix(const Matrix& m) : values_(m.values_), rows_(m.rows_), cols_(m.cols_)	{};
	Matrix(int rows, int cols) : rows_(rows), cols_(cols)	{ Resize(rows, cols); };

	int rows() const { return rows_; }
	int cols() const { return cols_; }
	void Resize(int rows, int cols)	{ values_.resize((rows_ = rows) * (cols_ = cols)); }

	T& operator()(int r, int c)	{ return values_[Sub2Ind(r, c)]; }
	const T& operator()(int r, int c) const	{ return values_[Sub2Ind(r, c)]; }

	Matrix operator+() const	{ return *this; }
	Matrix operator-() const
	{
		int num;
		Matrix<T> x(rows_, cols_);
		for (int i = 0; i < rows_; i++){
			for (int j = 0; j < cols_; j++){
				x(i, j) = -operator()(i, j);
			}
		}
		return x;
	}
	Matrix Transpose() const
	{
		Matrix<T> x(cols_, rows_);
		for (int i = 0; i < rows(); i++)
		{
			for (int j = 0; j < cols(); j++){
				x(j, i) = operator()(i, j);
			}
		}
		return x;
	}
private:
	// 행과 열을 받아 해당 원소의 배열 내의 위치를 계산하는 함수.
	int Sub2Ind(int r, int c) const { return r + c * rows_; }
	vector<T> values_;  // rows_ * cols_ 만큼의 정수를 가진 배열.
	int rows_, cols_;   // 행과 열의 개수.

	// operator 함수들을 friend 로 선언하지 말고 public 함수를 이용.
};

template <typename T>
istream& operator>>(istream& is, Matrix<T>& m)
{
	int a, b;
	cin >> a >> b;
	m.Resize(a, b);
	for (int i = 0; i < m.rows(); i++){
		for (int j = 0; j < m.cols(); j++){
			is >> m(i, j);
		}
	}
	return is;
}

template <typename T>
ostream& operator<<(ostream& os, const Matrix<T>& m)
{
	for (int i = 0; i < m.rows(); i++){
		for (int j = 0; j < m.cols(); j++){
			os << m(i, j) << " ";
		}
		os << endl;
	}
	return os;
}


template <typename T, typename S>
Matrix<T> operator+(const Matrix<T>& lm, const Matrix<S>& rm)
{
	Matrix<T> result(lm.rows(), lm.cols());
	if (lm.cols() != rm.cols() || lm.rows() != rm.rows()){
		cout << "Invalid operation" << endl;
		exit(0);
	}
	for (int i = 0; i < lm.rows(); i++){
		for (int j = 0; j < rm.cols(); j++){
			result(i, j) = lm(i, j) + rm(i, j);
		}
	}
	return result;
}
template <typename T, typename S>
Matrix<T> operator-(const Matrix<T>& lm, const Matrix<S>& rm)
{
	Matrix<T> result(lm.rows(), lm.cols());
	if (lm.cols() != rm.cols() || lm.rows() != rm.rows()){
		cout << "Invalid operation" << endl;
		exit(0);
	}
	for (int i = 0; i < lm.rows(); i++){
		for (int j = 0; j < rm.cols(); j++){
			result(i, j) = lm(i, j) - rm(i, j);
		}
	}
	return result;

}
template <typename T, typename S>
Matrix<T> operator*(const Matrix<T>& lm, const Matrix<S>& rm)
{
	Matrix<T> result(lm.rows(), rm.cols());
	if (lm.cols() != rm.rows()){
		cout << "Invalid operation" << endl;
		exit(0);
	}
	for (int i = 0; i < lm.rows(); i++){
		for (int j = 0; j < rm.cols(); j++){
			for (int k = 0; k < lm.cols(); k++){
				result(i, j) += lm(i, k)*rm(k, j);
			}
		}
	}
	return result;
}

template <typename T, typename S>
Matrix<T> operator+(const S& a, const Matrix<T>& rm)
{
	Matrix<T> result(rm.rows(), rm.cols());
	for (int i = 0; i < rm.rows(); i++){
		for (int j = 0; j < rm.cols(); j++){
			result(i, j) = a + rm(i, j);
		}
	}
	return result;
}
template <typename T, typename S>
Matrix<T> operator-(const S& a, const Matrix<T>& rm)
{
	Matrix<T> result(rm.rows(), rm.cols());
	for (int i = 0; i < rm.rows(); i++){
		for (int j = 0; j < rm.cols(); j++){
			result(i, j) = a - rm(i, j);
		}
	}
	return result;
}
template <typename T, typename S>
Matrix<T> operator*(const S& a, const Matrix<T>& rm)
{
	Matrix<T> result(rm.rows(), rm.cols());
	for (int i = 0; i < rm.rows(); i++){
		for (int j = 0; j < rm.cols(); j++){
			result(i, j) = a * rm(i, j);
		}
	}
	return result;
}
template <typename T, typename S>
Matrix<T> operator+(const Matrix<T>& lm, const S& a)
{
	Matrix<T> result(lm.rows(), lm.cols());
	for (int i = 0; i < lm.rows(); i++){
		for (int j = 0; j < lm.cols(); j++){
			result(i, j) = lm(i, j) + a;
		}
	}
	return result;
}
template <typename T, typename S>
Matrix<T> operator-(const Matrix<T>& lm, const S& a)
{
	Matrix<T> result(lm.rows(), lm.cols());
	for (int i = 0; i < lm.rows(); i++){
		for (int j = 0; j < lm.cols(); j++){
			result(i, j) = lm(i, j) - a;
		}
	}
	return result;
}
template <typename T, typename S>
Matrix<T> operator*(const Matrix<T>& lm, const S& a)
{
	Matrix<T> result(lm.rows(), lm.cols());
	for (int i = 0; i < lm.rows(); i++){
		for (int j = 0; j < lm.cols(); j++){
			result(i, j) = lm(i, j) * a;
		}
	}
	return result;
}

#endif